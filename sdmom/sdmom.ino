/******************************************************************************
 *  "SDMOM.ino"
 *
 *  Firmware for the Software-Defined Magneto-Optical Modulator
 *
 *  Copyright (C) 2018 by Jann Eike KRUSE <jann@commonslab.gr>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/


#include <LiquidCrystal.h>


//***** Global Constants *****
#define TRIGGER             P58   // Trigger output pin GPIO number
#define PWM_HALF_PERIOD     300   // 150kHz symmetric UP-DOWN PWM @ 90 MHz TBCLK = SYSCLKOUT /1 /1 (300 => 90MHz / 300 = 300kHz => 2*150kHz symmetric)

#define DI24_0              P0    // TAPAS Digital 24V Inuput on GPIO0
#define DI24_1              P1    // TAPAS Digital 24V Inuput on GPIO1
#define DI24_2              P2    // TAPAS Digital 24V Inuput on GPIO2
#define DI24_3              P3    // TAPAS Digital 24V Inuput on GPIO3

#define EQEP1A              P20    // TAPAS Quadrature Encoder 1 input A on GPIO20
#define EQEP1B              P21    // TAPAS Quadrature Encoder 1 input B on GPIO20
#define EQEP1I              P23    // TAPAS Quadrature Encoder 1 input I on GPIO20

#define EQEP2A              P54    // TAPAS Quadrature Encoder 2 input A on GPIO20
#define EQEP2B              P55    // TAPAS Quadrature Encoder 2 input B on GPIO20
#define EQEP2I              P56    // TAPAS Quadrature Encoder 2 input I on GPIO20

#define ADCI1               AA0    // TAPAS P1 current sensor on Analog A0
#define ADCI2               AA1    // TAPAS P2 current sensor on Analog A1
#define ADCI3               AA2    // TAPAS P3 current sensor on Analog A2
#define ADCIIn              AA3    // TAPAS DC input current sensor on Analog A30

#define ADCV1               AA4    // TAPAS P1 voltage sensor on Analog A4
#define ADCV2               AA5    // TAPAS P1 voltage sensor on Analog A5
#define ADCV3               AA6    // TAPAS P1 voltage sensor on Analog A6
#define ADCVIn              AA7    // TAPAS DC input voltage sensor on Analog A7


//***** Global Variables *****
Int16  pwmPer300     = 0;             // -300 => -DCin // +300 => +DCin
Int16  pwmPer300Max  = 30;            // How far (+/-) to ramp up/down.
Int16  pwmPer300Limit= 120;           // Max. user-selectable limit.
Uint16 pwmStepDelay  = 300;           // How fast to ramp up/down (clicks)
Uint16 pwmMaxDelay   = 500;           // How long to wait at max PWM. (ms)
//bool   delayAtZero   = false;         // To wait or not to wait at zero field.
                                      //
enum DelayModeType {
  DELAY_AT_PEAKS,
  DELAY_AT_ZERO,
  DELAY_WAIT_SERIAL
};

enum StatusType {
  STATUS_NONE,
  STATUS_RUN,
  STATUS_SET
};

DelayModeType delayMode = DELAY_AT_PEAKS;
StatusType status = STATUS_NONE;
char lcdBuffer[20] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void setup() {
    Serial.begin(9600);
    setupPWM();
    setupGPIO();
} // setup

void loop() {
  LiquidCrystal lcd(P19, P16, P25, P27, P24, P26); // This must not be global,
  // LCD connector  RS   En    4    5    6    7    // otherwise it does not boot.
  // TAPAS connector 6    5    4    3    2    1
  lcd.begin(16, 2);

  while (true==true) {
    if (digitalRead(EQEP1I)==LOW) {    //If green button pressed/latched...
      if (status != STATUS_SET) {
        status = STATUS_SET;
        lcd.setCursor(0, 0);           //Show on first row, after 0 spaces...
        lcd.print("AMPLI:    DELAY:"); //...the titles.
        ShowParameters(lcd);
      }
      if (digitalRead(EQEP2I)==LOW) {           //If yellow button pressed...
        if      (delayMode == DELAY_AT_PEAKS)    {delayMode = DELAY_AT_ZERO     ;} //...cycle between...
        else if (delayMode == DELAY_AT_ZERO)     {delayMode = DELAY_WAIT_SERIAL ;} //...available...
        else if (delayMode == DELAY_WAIT_SERIAL) {delayMode = DELAY_AT_PEAKS    ;} //...options.
        ShowParameters(lcd);
        delay(300);
      }
      if (digitalRead(EQEP1A)==LOW) {           //If blue [-] button pressed...
        if (pwmPer300Max > 0) {pwmPer300Max--;} //...reduce amplitude.
        ShowParameters(lcd);
        delay(100);
      }
      if (digitalRead(EQEP1B)==LOW) {             //If red [+] button pressed...
        if (pwmPer300Max < pwmPer300Limit) {pwmPer300Max++;} //...increase amplitude.
        ShowParameters(lcd);
        delay(100);
      }
      if (digitalRead(EQEP2A)==LOW) {           //If black [-] button pressed...
        if (pwmMaxDelay > 30) {
          if ((pwmMaxDelay >     0) && (pwmMaxDelay <=   200)) {pwmMaxDelay=pwmMaxDelay-  1;}
          if ((pwmMaxDelay >   200) && (pwmMaxDelay <=  2000)) {pwmMaxDelay=pwmMaxDelay- 10;}
          if ((pwmMaxDelay >  2000) && (pwmMaxDelay <= 60000)) {pwmMaxDelay=pwmMaxDelay-100;}
        } //...reduce delay.
        ShowParameters(lcd);
        delay(100);
      }
      if (digitalRead(EQEP2B)==LOW) {             //If white [+] button pressed...
        if (pwmMaxDelay < 60000) {
          if ((pwmMaxDelay >=     1) && (pwmMaxDelay <   200)) {pwmMaxDelay=pwmMaxDelay+  1;}
          if ((pwmMaxDelay >=   200) && (pwmMaxDelay <  2000)) {pwmMaxDelay=pwmMaxDelay+ 10;}
          if ((pwmMaxDelay >=  2000) && (pwmMaxDelay < 60000)) {pwmMaxDelay=pwmMaxDelay+100;}
        } //...increase delay.
        ShowParameters(lcd);
        delay(100);
      }

    } else {                               //If green button NOT pressed/latched...
      if (status != STATUS_RUN) {
        status = STATUS_RUN;
        lcd.setCursor(0, 0);               //Show on second row, after 0 spaces...
        lcd.print("RUN             ");     //...the status: running
        ShowParameters(lcd);
      }
      SendPulse();

      //Test serial port:
      Serial.print("*");

    } // end if button (NOT) pressed
  } // end while (true==true)
} // end loop()

Int16 ShowParameters(LiquidCrystal& lcd) {
  lcd.setCursor(0, 1);           //Show on second row, after 0 spaces...
  sprintf(lcdBuffer, "%3d", (int)(pwmPer300Max/3.0));
  lcd.print(lcdBuffer);          //...the current amplitude in percent (first digits)...
  lcd.print(".");
  sprintf(lcdBuffer, "%1d", (int)((pwmPer300Max/3.0)*10)%10);
  lcd.print(lcdBuffer);          //...and decimal fraction digit.
  lcd.print("%");

  if (delayMode == DELAY_AT_ZERO) {
    lcd.setCursor(7, 1);           //Show on second row, after 7 spaces...
    lcd.print("0");
  } else if (delayMode == DELAY_AT_PEAKS) {
    lcd.setCursor(7, 1);           //Show on second row, after 7 spaces...
    lcd.print("^");
  } else if (delayMode == DELAY_WAIT_SERIAL) {
    lcd.setCursor(7, 1);           //Show on second row, after 7 spaces...
    lcd.print("S");
  }

  lcd.setCursor(9, 1);           //Show on second row, after 9 spaces...
  sprintf(lcdBuffer, "%5u", (pwmMaxDelay));
  lcd.print(lcdBuffer);          //...and decimal fraction digit.
  lcd.print("ms");
  return 0;
}

Int16 SendPulse() {
    // ***** POSITIVE CYCLE *****
    // Increase output voltage towards positive (up to pwmPer300Max):
    for (pwmPer300=0; pwmPer300<pwmPer300Max; pwmPer300=pwmPer300+pwmPer300Max/20+1) {
        SetPWM(pwmPer300);
        for(int i=0; i<pwmStepDelay; i++) {asm(" NOP");} // wait
    }
    SetPWM(pwmPer300Max);

    // Wait a bit at max.:
    delay(20);

    // Raise trigger:
    digitalWrite(TRIGGER, HIGH);

    // Toggle LEDs:
    digitalWrite(RED_LED, HIGH);
    digitalWrite(BLUE_LED, HIGH);

    // Wait the rest at max.: (Unless we're waiting for a message from the serial port!)
    if (delayMode == DELAY_WAIT_SERIAL) while (Serial.read() != 0) delay(1); // Delay until the NULL (0x00) character is read from the serial. 
    else delay(pwmMaxDelay-20);

/*
    delay(1000);
    Serial.print(analogRead(ADCVIn));
    Serial.print("\t");
    Serial.print(analogRead(ADCV1));
    Serial.print("\t");
    Serial.print(analogRead(ADCV2));
    Serial.print("\t");
    Serial.print(analogRead(ADCV3));
    Serial.print("\t");
    Serial.print("\t");
    Serial.print(analogRead(ADCIIn));
    Serial.print("\t");
    Serial.print(analogRead(ADCI1));
    Serial.print("\t");
    Serial.print(analogRead(ADCI2));
    Serial.print("\t");
    Serial.print(analogRead(ADCI3));
    Serial.print("\t");
    Serial.print("\t");
    delay(1000);
/**/
      // Decrease output voltage towards zero:
    for (pwmPer300=pwmPer300Max; pwmPer300>0; pwmPer300=pwmPer300-pwmPer300Max/20-1) {
        SetPWM(pwmPer300);
        for(int i=0; i<pwmStepDelay; i++) {asm(" NOP");}// wait
    }
    SetPWM(0);

    if (delayMode == DELAY_AT_ZERO) {
      // Wait a bit at zero:
      delay(20);

      // Release trigger:
      digitalWrite(TRIGGER, LOW);

      // Toggle LEDs:
      digitalWrite(RED_LED, HIGH);
      digitalWrite(BLUE_LED, LOW);

        // Wait the rest at zero:
      delay(pwmMaxDelay-20);
    }

    // ***** NEGATIVE CYCLE *****
    // Decrease output voltage towards negative (up to 99% -DCin):
    for (pwmPer300=0; pwmPer300 > -pwmPer300Max; pwmPer300=pwmPer300-pwmPer300Max/20-1) {
        SetPWM(pwmPer300);
        for(int i=0; i<pwmStepDelay; i++) {asm(" NOP");}// wait
    }
    SetPWM(-pwmPer300Max);

    // Wait a bit at min.:
    delay(20);

    if (delayMode == DELAY_AT_ZERO) { // Take care of different trigger modes...
      // Raise trigger again:
      digitalWrite(TRIGGER, HIGH);
    } else {
      // Release trigger:
      digitalWrite(TRIGGER, LOW);
    }

    // Toggle LEDs:
    digitalWrite(RED_LED, LOW);
    digitalWrite(BLUE_LED, HIGH);

    // Wait the rest at min.:
    if (delayMode == DELAY_WAIT_SERIAL) while (Serial.read() != 1) delay(1); // Delay until the START_OF_HEADING (0x01) character is read from the serial. 
    else delay(pwmMaxDelay-20);

/*
    delay(1000);
    Serial.print(analogRead(ADCVIn));
    Serial.print("\t");
    Serial.print(analogRead(ADCV1));
    Serial.print("\t");
    Serial.print(analogRead(ADCV2));
    Serial.print("\t");
    Serial.print(analogRead(ADCV3));
    Serial.print("\t");
    Serial.print("\t");
    Serial.print(analogRead(ADCIIn));
    Serial.print("\t");
    Serial.print(analogRead(ADCI1));
    Serial.print("\t");
    Serial.print(analogRead(ADCI2));
    Serial.print("\t");
    Serial.print(analogRead(ADCI3));
    Serial.print("\t");
    Serial.print("\n");
    delay(1000);
/**/
    // Increase output voltage towards zero:
    for (pwmPer300=-pwmPer300Max; pwmPer300<0; pwmPer300=pwmPer300+pwmPer300Max/20+1) {
        SetPWM(pwmPer300);
        for(int i=0; i<pwmStepDelay; i++) {asm(" NOP");}// wait
    }
    SetPWM(0);

    if (delayMode == DELAY_AT_ZERO) {
      // Wait again a bit at zero:
      delay(20);

      // Release trigger:
      digitalWrite(TRIGGER, LOW);

      // Toggle LEDs:
      digitalWrite(RED_LED, LOW);
      digitalWrite(BLUE_LED, LOW);

        // Wait the rest at zero:
      delay(pwmMaxDelay-20);
    }

  return 0;
}

void SetPWM(Int16 per300) {
    Int16 compareValuePositive = 0;
    Int16 compareValueNegative = 0;

    if (per300 > 0) {
        compareValuePositive = per300;
        compareValueNegative = 0;
    }

    if (per300 < 0) {
        compareValuePositive = 0;
        compareValueNegative = -per300;
    }

    // Never go to full on PWM! (The FET driver's bootstrap will fail.)
    if (per300 < -295) {
        compareValuePositive = 0;
        compareValueNegative = 295;
    }

    if (per300 > 295) {
        compareValuePositive = 295;
        compareValueNegative = 0;
    }

    // Set PWM duty cycles
    EPwm4Regs.CMPA.half.CMPA = 0;
    EPwm5Regs.CMPA.half.CMPA = compareValuePositive;
    EPwm6Regs.CMPA.half.CMPA = compareValueNegative;

} // end SetPWM


void setupGPIO(void) {
    //Trigger and LEDs can use normal Energia (Ardino) functions:
    digitalWrite( RED_LED, LOW);
    pinMode(      RED_LED, OUTPUT);

    digitalWrite(BLUE_LED, LOW);
    pinMode(     BLUE_LED, OUTPUT);

    digitalWrite( TRIGGER, LOW);
    pinMode(      TRIGGER, OUTPUT);

    //TAPAS Digital 24V inputs (DI24):
    pinMode(DI24_0, INPUT);
    pinMode(DI24_1, INPUT);
    pinMode(DI24_2, INPUT);
    pinMode(DI24_3, INPUT);

    //TAPAS (Enhanced) Quadrature Encoders Port inputs (EQEP):
    pinMode(EQEP1A, INPUT);
    pinMode(EQEP1B, INPUT);
    pinMode(EQEP1I, INPUT);
    pinMode(EQEP2A, INPUT);
    pinMode(EQEP2B, INPUT);
    pinMode(EQEP2I, INPUT);

    // Power outputs (P1_OUT, P2_OUT, P3_OUT) on TAPAS need ePWM:
    asm(" EALLOW");             // Allow access to protected register

    // P1_OUT
    GpioCtrlRegs.GPAMUX1.bit.GPIO6  = 0;    // 0=GPIO
    GpioCtrlRegs.GPAMUX1.bit.GPIO7  = 0;    // 0=GPIO

    // P2_OUT
    GpioCtrlRegs.GPAMUX1.bit.GPIO8  = 1;    // 1=EPWM5A
    GpioCtrlRegs.GPAMUX1.bit.GPIO9  = 1;    // 1=EPWM5B

    // P3_OUT
    GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 1;    // 1=EPWM6A
    GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 1;    // 1=EPWM6B

    //Fixate other pull-up / pull-down GaN FETs:
    GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;          // P1 HIGH-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO6 = 0;          // P1 HIGH-SIDE => OFF

    GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;          // P1  LOW-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO7 = 1;          // P1  LOW-SIDE => ON (PULL-DOWN)

    GpioCtrlRegs.GPADIR.bit.GPIO8 = 1;          // P2 HIGH-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO8 = 0;          // P2 HIGH-SIDE => OFF

    GpioCtrlRegs.GPADIR.bit.GPIO9 = 1;          // P2  LOW-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO9 = 1;          // P2  LOW-SIDE => ON (PULL-DOWN)

    GpioCtrlRegs.GPADIR.bit.GPIO10 = 1;         // P3 HIGH-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO10 = 0;         // P3 HIGH-SIDE => OFF

    GpioCtrlRegs.GPADIR.bit.GPIO11 = 1;         // P3  LOW-SIDE => OUTPUT
    GpioDataRegs.GPASET.bit.GPIO11 = 1;         // P3  LOW-SIDE => ON (PULL-DOWN)

    asm(" EDIS");   // Disable access to protected registers

} // end setupGPIO


void setupPWM(void) {
// Disable ePWM clock to start them syncronized:
    asm(" EALLOW");                         // Allow protected register access
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    asm(" EDIS");                           // Disallow protected register access


    // Set up Deadband module for ePWM4 (Channel P1):
    EPwm4Regs.DBCTL.bit.POLSEL = 0b10;   // Active Hi complementary: ePWMB goes HIGH if-and-only-if ePWMA goes LOW
    EPwm4Regs.DBRED = 4;                 // Rising-Edge  Delay (RED): Clicks to delay *ePWM4A* when ePWMA is going HIGH (e.g. 4 clicks @TBCLK=90MHz => 44ns)
    EPwm4Regs.DBFED = 4;                 // Falling-Edge Delay (FED): Clicks to delay *ePWM4B* when ePWMA is going LOW
    EPwm4Regs.DBCTL.bit.OUT_MODE = 0b11; // enable Dead-band module

    // Set up Deadband module for ePWM5 (Channel P2):
    EPwm5Regs.DBCTL.bit.POLSEL = 0b10;   // Active Hi complementary
    EPwm5Regs.DBRED = 4;                 // RED = delay *ePWM5A* when ePWMA is going HIGH
    EPwm5Regs.DBFED = 4;                 // FED = delay *ePWM5B* when ePWMA is going LOW
    EPwm5Regs.DBCTL.bit.OUT_MODE = 0b11; // enable Dead-band module

    // Set up Deadband module for ePWM6 (Channel P3):
    EPwm6Regs.DBCTL.bit.POLSEL = 0b10;   // Active Hi complementary
    EPwm6Regs.DBFED = 4;                 // FED = EPWM_MIN_DB_FE TBCLKs
    EPwm6Regs.DBRED = 4;                 // RED = EPWM_MIN_DB TBCLKs
    EPwm6Regs.DBCTL.bit.OUT_MODE = 0b11; // enable Dead-band module


    // ePWM4 (Channel P1_OUT): Symmetric PWM on EPWM4A and EPWM4B pins
    EPwm4Regs.TBCTL.bit.CTRMODE = 0x3;      // Disable the timer

    EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0b000;  // Set PWM counter clock...
    EPwm4Regs.TBCTL.bit.CLKDIV = 0b000;     // ...to system clock (e.g. 90MHz)
    EPwm4Regs.TBCTR = 0x0000;               // Clear timer counter
    EPwm4Regs.TBPRD = PWM_HALF_PERIOD;      // Set timer period
    EPwm4Regs.TBPHS.half.TBPHS = 0x0000;    // Set timer phase
    EPwm4Regs.CMPA.half.CMPA = 0;           // Set PWM duty cycle
    EPwm4Regs.CMPCTL.bit.LOADAMODE = 0b10;  // load on zero or PRD match

    // POSITIVE ePWM4A (higher compare value -> longer duty-cycle)
    EPwm4Regs.AQCTLA.bit.CAD = 0b10; // CounterA-match Down-conting: set  (HIGH)
    EPwm4Regs.AQCTLA.bit.CAU = 0b01; // CounterA-match   Up-conting: clear (LOW)

    // Do nothing on output ePWM4B (Since IT'S CONTRL'D BY THE DEAD-BAND MODULE)
    EPwm4Regs.AQCTLB.bit.CAD = 0b00;
    EPwm4Regs.AQCTLB.bit.CAU = 0b00;

    EPwm4Regs.PCCTL.bit.CHPEN = 0;		// Disable PWM chopper
    EPwm4Regs.TZDCSEL.all = 0x0000;		// Disable trip zone and DC compare

    EPwm4Regs.TBCTL.bit.CTRMODE = 0x2;	// Enable the timer in up/down mode


    // ePWM5 (Channel P2_OUT): Symmetric PWM on EPWM5A and EPWM5B pins
    EPwm5Regs.TBCTL.bit.CTRMODE = 0x3;      // Disable the timer

    EPwm5Regs.TBCTL.bit.HSPCLKDIV = 0b000;  // Set PWM counter clock...
    EPwm5Regs.TBCTL.bit.CLKDIV = 0b000;     // ...to system clock (e.g. 90MHz)
    EPwm5Regs.TBCTR = 0x0000;               // Clear timer counter
    EPwm5Regs.TBPRD = PWM_HALF_PERIOD;      // Set timer period
    EPwm5Regs.TBPHS.half.TBPHS = 0x0000;    // Set timer phase
    EPwm5Regs.CMPA.half.CMPA = 0;           // Set PWM duty cycle
    EPwm5Regs.CMPCTL.bit.LOADAMODE = 0b10;  // load on zero or PRD match

    // POSITIVE ePWM5A (higher compare value -> longer duty-cycle)
    EPwm5Regs.AQCTLA.bit.CAD = 0b10; // CounterA-match Down-conting: set  (HIGH)
    EPwm5Regs.AQCTLA.bit.CAU = 0b01; // CounterA-match   Up-conting: clear (LOW)

    // Do nothing on output ePWM5B (Since IT'S CONTRL'D BY THE DEAD-BAND MODULE)
    EPwm5Regs.AQCTLB.bit.CAD = 0b00;
    EPwm5Regs.AQCTLB.bit.CAU = 0b00;

    EPwm5Regs.PCCTL.bit.CHPEN = 0;      // Disable PWM chopper
    EPwm5Regs.TZDCSEL.all = 0x0000;     // Disable trip zone and DC compare
    EPwm5Regs.TBCTL.bit.CTRMODE = 0x2;  // Enable the timer in up/down mode


    // ePWM6 (Channel P3_OUT): Symmetric PWM on EPWM6A and EPWM6B pins
    EPwm6Regs.TBCTL.bit.CTRMODE = 0x3;      // Disable the timer

    EPwm6Regs.TBCTL.bit.HSPCLKDIV = 0b000;  // Set PWM counter clock...
    EPwm6Regs.TBCTL.bit.CLKDIV = 0b000;     // ...to system clock (e.g. 90MHz)
    EPwm6Regs.TBCTR = 0x0000;               // Clear timer counter
    EPwm6Regs.TBPRD = PWM_HALF_PERIOD;      // Set timer period
    EPwm6Regs.TBPHS.half.TBPHS = 0x0000;    // Set timer phase
    EPwm6Regs.CMPA.half.CMPA = 0;           // Set PWM duty cycle
    EPwm6Regs.CMPCTL.bit.LOADAMODE = 0b10;  // load on zero or PRD match

    // POSITIVE ePWM6A (higher compare value -> longer duty-cycle)
    EPwm6Regs.AQCTLA.bit.CAD = 0b10; // CounterA-match Down-conting: set  (HIGH)
    EPwm6Regs.AQCTLA.bit.CAU = 0b01; // CounterA-match   Up-conting: clear (LOW)

    // Do nothing on output ePWM6B (Since IT'S CONTRL'D BY THE DEAD-BAND MODULE)
    EPwm6Regs.AQCTLB.bit.CAD = 0b00;
    EPwm6Regs.AQCTLB.bit.CAU = 0b00;

    EPwm6Regs.PCCTL.bit.CHPEN = 0;      // Disable PWM chopper
    EPwm6Regs.TZDCSEL.all = 0x0000;     // Disable trip zone and DC compare
    EPwm6Regs.TBCTL.bit.CTRMODE = 0x2;  // Enable the timer in up/down mode


    // Lastly, enable clock again to syncronize all PWMs.
    asm(" EALLOW");                         // Allow access to protected register
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;  // Enable TBCLK for ePWM modules
    asm(" EDIS");                           // Disable access to protected registers

} // end setupPWM







